import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
      alignment: Alignment.center,
      children: <Widget>[
        // background image and bottom contents
        Column(
          children: <Widget>[
            Container(
              height: 220.0,
              color: Colors.orange[100],
              child: Column(
                children: [
                  SizedBox(
                    height: 30.0,
                  ),
                  ListTile(
                    title: Center(child: Text("My Profile",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)),
                    leading: InkWell(
                        onTap: (){
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black,
                        )
                    ),
                    trailing: Icon(Icons.edit, color: Colors.black,),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                height: 200.0,
                color: Colors.white,
                child: ListView(
                  children: [
                    SizedBox(
                      height: 150.0,
                    ),
                    Row(
                      children: [
                        SizedBox(width: 20.0,),
                        Text("Name",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: TextField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SizedBox(width: 20.0,),
                        Text("Email Address",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: TextField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 20.0,
                    ),

                    Row(
                      children: [
                        SizedBox(width: 20.0,),
                        Text("Mobile Number",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                      ],
                    ),

                    Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 10, right: 20),
                        child: Row(
                          //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [

                            Container(
                              width: 150,
                              child: Expanded(
                                child: CountryCodePicker(
                                  initialSelection: 'IN',
                                  showCountryOnly: true,
                                  showOnlyCountryWhenClosed: false,
                                  alignLeft: true,
                                ),
                              ),
                            ),
                            Expanded(
                                child: TextField(
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                  ),
                                ),
                            )
                          ],
                        )),
                    Container(
                      height: 65,
                      padding:EdgeInsets.only(left: 20,right: 20,top: 10),
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: Colors.deepOrange,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(6.0)
                        ),
                        onPressed: (){},
                        child: Text("Save"),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        // Profile image
        Positioned(
          top: 140.0, // (background container size) - (circle height / 2)
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(
                  "https://media.istockphoto.com/photos/portrait-of-relaxed-young-man-with-bluetooth-headphones-in-forest-picture-id1286401346?s=2048x2048",
                ),
                radius: 80.0,
              ),
              SizedBox(height: 10.0,),
              Text( "dhdhdgdgd ",style: TextStyle(fontSize: 19.0, ),)
            ],
          ),
        )
      ],
    )
    );
  }
}
