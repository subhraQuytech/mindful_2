import 'package:flutter/material.dart';
import 'package:mindful_2/screen/reset_password_page.dart';
import 'package:mindful_2/screen/sign_in.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          InkWell(
            onTap: (){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignIn()));
            },
            child: ListTile(
              leading: Icon(Icons.arrow_back_ios, color: Colors.black,),
            ),
          ),
          getImageAsset(),
          SizedBox(
            height: 20.0,
          ),
          Center(child: Text("Forgot Password", style: TextStyle(color: Colors.black, fontSize: 25.0),)),

          SizedBox(
            height: 20.0,
          ),
          Center(child: Container(
            width: 250.0,
              child: Text("We just need your 10 digit mobile number to send you a code",
                style: TextStyle(color: Colors.black45, fontSize: 15.0),
                textAlign: TextAlign.center,
              ))
          ),

        SizedBox(
          height: 80.0,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Text("Mobile Number", style:  TextStyle(color: Colors.grey[600], fontWeight: FontWeight.bold, fontSize: 16.0),),
        ),
          Padding(
              padding: EdgeInsets.only(
                top: 5,
                left: 20,
                right: 20.0
              ),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3))),
              )
          ),
          SizedBox(
            height: 0.0,
          ),
          ListTile(
            trailing: Text(
              "Get OTP on your email",
              style: TextStyle(
                color: Colors.orange[800],
                fontSize: 16.0,
                fontWeight: FontWeight.bold
              ),
            ),
          ),

          SizedBox(
            height: 30,
          ),
          Container(
            height: 75,
            child: Padding(
              padding: const EdgeInsets.only(top: 28.0,left: 20.0, right: 20.0),
              child: RaisedButton(
                color: Colors.orange[800],
                onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ResetPasswordPage()));

                },
                child: Center(
                  child: Text("Submit",style: TextStyle(color: Colors.white, fontSize: 18.0),),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 150,
      height: 150,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 60),
    );
  }
}
