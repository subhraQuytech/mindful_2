import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[100],
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          ListTile(
            leading: Icon(Icons.arrow_back_ios, color: Colors.black,),
            title: Center(
              child: Text("Notification", style: TextStyle(
                fontSize: 20.0,
                color: Colors.black,
                fontWeight: FontWeight.bold
              ),
              ),
            ),
            trailing: Icon(Icons.arrow_back_ios, color: Colors.orange[100],),
          )
        ],
      ),
    );
  }
}
