
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mindful_2/block/contact_us_bloc.dart';
import 'package:mindful_2/screen/settings_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ContactUsPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ContactUsPageState();
  }

}

class _ContactUsPageState extends State<ContactUsPage> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  var name_txt = TextEditingController();
  var email_txt = TextEditingController();
  var message_txt = TextEditingController();

  void addContactUsListener() {
    contactUsBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SettingsPage()));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }

  @override
  void initState() {
    addContactUsListener();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [

          ListTile(
            title: Center( child:Text("Contact Us",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),)),
            leading: InkWell(
                onTap: (){
                 // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Signup()));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,

                )
            ),
            trailing: Icon(Icons.edit, color: Colors.white12,),
          ),
          Padding(padding: EdgeInsets.only(top: 50)),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20),
            child: Text("Name", style: TextStyle(
              fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: TextField(
              controller: name_txt,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
    ),
                ),

            ),
          ),



          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: Text("Email", style: TextStyle(
                fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),

            child: TextField(
              controller: email_txt,
              keyboardType: TextInputType.emailAddress,
            //  controller: passwordController,
              decoration: InputDecoration(

               border: OutlineInputBorder(
               borderRadius: BorderRadius.circular(5),
            ),

         ),

            ),
          ),


          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: Text("Message", style: TextStyle(
                fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            height: 170,
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: TextFormField(
              controller: message_txt,
              minLines: 10,
              maxLines: 16,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintStyle: TextStyle(
                    color: Colors.grey
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
              ),
            ),


 //             controller: passwordController,
            /*  decoration: InputDecoration(
                hintText: "Enter a message",
                filled: true,
                  border: OutlineInputBorder(
                 borderRadius: BorderRadius.circular(5),


             ),
                ),*/
          ),
          SizedBox(height: 60,),
          Container(
            height: 65,
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(6.0)
              ),
              onPressed: (){

                _counter = _prefs.then((SharedPreferences prefs) {

                  contactUsBloc.contactUs(name_txt.text, email_txt.text, message_txt.text, prefs.getString("token").toString());

                  return 0;

                });

              },
              child: Text("Confirm"),
            ),
          )

        ],
      ),
    );
  }
}