import 'package:flutter/material.dart';
import 'package:mindful_2/screen/forgot_password_page.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          InkWell(
            onTap: (){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ForgotPasswordPage()));
            },
            child: ListTile(
              leading: Icon(Icons.arrow_back_ios, color: Colors.black,),
            ),
          ),
          getImageAsset(),
          SizedBox(
            height: 20.0,
          ),
          Center(child: Text("Reset Password", style: TextStyle(color: Colors.black, fontSize: 25.0),)),

          SizedBox(
            height: 20.0,
          ),
          Center(child: Container(
              width: 250.0,
              child: Text("We have sent a 4 digit code on your registered Email",
                style: TextStyle(color: Colors.black45, fontSize: 15.0),
                textAlign: TextAlign.center,
              ))
          ),

          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 100.0, right: 100.0),
            child: OTPTextField(
              length: 4,
              outlineBorderRadius: 5,
              width: 20,
              fieldStyle: FieldStyle.box,
            ),
          ),

          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Not Received?",style: TextStyle(fontSize: 16.0),),
              SizedBox(width: 10,),
              Text("Resend", style :TextStyle(color: Colors.orange, fontSize: 16.0)),
            ],
          ),

          SizedBox(
            height: 30.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text("New Password", style:  TextStyle(color: Colors.grey[600], fontWeight: FontWeight.bold, fontSize: 16.0),),
          ),
          Padding(
              padding: EdgeInsets.only(
                  top: 5,
                  left: 20,
                  right: 20.0
              ),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3))),
              )
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text("Confirm Password", style:  TextStyle(color: Colors.grey[600], fontWeight: FontWeight.bold, fontSize: 16.0),),
          ),
          Padding(
              padding: EdgeInsets.only(
                  top: 5,
                  left: 20,
                  right: 20.0
              ),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3))),
              )
          ),

          SizedBox(
            height: 30,
          ),
          Container(
            height: 75,
            child: Padding(
              padding: const EdgeInsets.only(top: 28.0,left: 20.0, right: 20.0),
              child: RaisedButton(
                color: Colors.orange[800],
                onPressed: () {  },
                child: Center(
                  child: Text("Update Password",style: TextStyle(color: Colors.white, fontSize: 18.0),),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 150,
      height: 150,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 60),
    );
  }
}
