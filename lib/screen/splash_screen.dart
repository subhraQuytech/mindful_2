import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mindful_2/screen/home_screen.dart';
import 'package:mindful_2/screen/sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;


  @override
  void initState() {

    _counter = _prefs.then((SharedPreferences prefs) {

      Timer(
          Duration(seconds: 3),
              () {

            if(prefs.getBool("isLoggedIn") == true){
              //ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Signin page")));
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));

            }else{
              //ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("home page")));
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignIn()));
              //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));

            }
          });
      return 0;

    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: getImageAsset()
      ),
    );
  }


  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 260,
      height: 260,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 0),
    );
  }
}
