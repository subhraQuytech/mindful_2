import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:mindful_2/block/sign_up_bloc.dart';
import 'package:mindful_2/screen/home_screen.dart';
import 'package:mindful_2/screen/sign_in.dart';



class Signup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignupState();
  }
}

class _SignupState extends State<Signup> {
  bool _isObscure = true;
  bool value = false;

  var name_txt = TextEditingController();
  var email_txt = TextEditingController();
  var mobile_txt = TextEditingController();
  var password_txt = TextEditingController();

  void signUp(String name, String email,String mobile,String country_code, String password){
    signUpBloc.signUp("rahul", "rahul@gmail.com", "+91", "8011299887", "12345","aaa","A1234567890#","A1234567890#","1");
  }

  void addSignUpListener() {
    signUpBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {
          setState(() {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
          });
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }

  @override
  void initState() {
    super.initState();
    addSignUpListener();
  }

  @override
  void dispose() {
    addSignUpListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //  TextStyle textStyle =Theme.of(context).textTheme.subtitle1
    final iskeybord = MediaQuery.of(context).viewInsets.bottom != 0;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(20),
            // EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                getImageAsset(),
                /*Text(
                  "DIVINE",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.deepOrangeAccent),
                ),
                Text(
                  "SOULYOGA",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.deepOrangeAccent),
                ),*/
                Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      "Sign Up",
                      style: TextStyle(fontSize: 25, color: Colors.black),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: Row(
                      children: [
                        Text(
                          "Name",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: TextField(
                      controller: name_txt,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(3))),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Row(
                      children: [
                        Text(
                          "Email",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                    ),
                    child: TextField(
                      controller: email_txt,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(3))),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Row(
                      children: [
                        Text(
                          "Mobile Number",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                  //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [

                        Expanded(
                              child: CountryCodePicker(
                                initialSelection: 'IN',
                                showCountryOnly: true,
                                showOnlyCountryWhenClosed: false,
                                alignLeft: true,



                              ),
                        /*  boxDecoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(3),
                              )
                          ),*/
                           /* child: TextField(
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(3))),
                        )*/
                        ),

                     Container(padding: EdgeInsets.only(left: 20),),
                        Expanded(
                            child: TextField(
                              controller: mobile_txt,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(3))),
                        ))
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Row(
                      children: [
                        Text(
                          "Password",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                      top: 5,
                    ),
                    child: TextField(
                      controller: password_txt,
                      obscureText: _isObscure,
                      // keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              _isObscure
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            },
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(3)
                          )),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 10),
                    child:Row(children: [
                      Checkbox(
                      //  title:  Text("title text"),
                        value: this.value,
                        onChanged: (bool? value) {
                          setState(() {
                            this.value = value!;
                          });
                        },
                      ),

                      Expanded(child: Text("By signing up you agree the Terms & Condition and Privacy Policy."
                          ,overflow: TextOverflow.fade))
                    ],)
                ),
/*
            Padding(
                padding: EdgeInsets.only(top: 10,bottom: 10),
                child:  TextField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border:OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3)
                      )
                  ),
                )),

            Padding(
                padding: EdgeInsets.only(top: 5,bottom: 5),
                child: Row(children: [
                    TextField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: 'Rate of Intrest',
                          hintText: 'In percent',
                          border:OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5)
                          )
                      ),
                    ),
                ])),
            Padding(
                padding: EdgeInsets.only(top: 5,bottom: 5),
                child:Row(
                  children: <Widget>[
                    Expanded(child: TextField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: 'Term',
                          hintText: 'Time in  years',
                          border:OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5)
                          )
                      ),
                    )),
                    Container(width: 5*5,),
                  ],
                )),*/
                Padding(
                    padding: EdgeInsets.only(bottom: 5, top: 5),
                    child: Row(
                      children: <Widget>[
                        ButtonTheme(minWidth: 50,height: 50,
                            child: Expanded(

                          child: RaisedButton(
                            elevation: 5.0,
                            color: Colors.deepOrange,
                            shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.circular(2.0),
                            ),
                            child: Text(
                              'Signup',
                              style: TextStyle(color: Colors.white,),
                            ),
                            onPressed: () {
                              signUp(name_txt.text, email_txt.text, mobile_txt.text, "+91", password_txt.text);
                            },
                          ),
                        )),

                      ],
                    )),
              ],
            ),
          ),
        ));
  }


  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 200,
      height: 200,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 60),
    );
  }
}
