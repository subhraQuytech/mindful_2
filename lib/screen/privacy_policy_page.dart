import 'package:flutter/material.dart';
import 'package:mindful_2/block/privacy_policy_bloc.dart';
import 'package:mindful_2/model/privacy_policy_Model.dart';
import 'package:mindful_2/screen/settings_page.dart';

class PrivacyPolicyPage extends StatefulWidget {
  const PrivacyPolicyPage({Key? key}) : super(key: key);

  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {

  @override
  void initState() {
    privacyPolicyBloc.getPrivacyPolicy();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: StreamBuilder<PrivacyPolicyModel>(
          stream: privacyPolicyBloc.subject.stream,
          builder: (context, snapshot) {
            if(snapshot.hasData){
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 30.0,
                  ),
                  ListTile(
                    title: Center(child: Text("Privacy Policy",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 20),)),
                    leading: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));

                      },
                        child: Icon(Icons.arrow_back_ios,color: Colors.black,)),
                    trailing: Icon(Icons.arrow_back_ios,color: Colors.white,),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Expanded(child: Container(child: Text(snapshot.data!.data!.description.toString())))
                ],
              );
            }else{
              return _errorShimmer();
            }
          }
        ),
      ),
    );
  }

  Widget _errorShimmer() {
    return Center(child: Text("Loading... !!"),);
  }
}
