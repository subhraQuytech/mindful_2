import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mindful_2/screen/MyState.dart';
import 'package:mindful_2/screen/settings_page.dart';
import 'package:mindful_2/screen/sign_up.dart';

class Buddies extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BuddiesState();
  }

}

class _BuddiesState extends State<Buddies> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
/*appBar: AppBar(
  centerTitle: true ,
  title: Text("Buddies",style: TextStyle(color: Colors.black),),
  automaticallyImplyLeading: false,
  backgroundColor: Colors.white,

),*/
      body: ListView(
            children:[
              ListTile(
               title: Center( child:Text("Buddies",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),)),
                leading: InkWell(
                    onTap: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SettingsPage()));
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    )
                ),
                trailing: Icon(Icons.edit, color: Colors.white12,),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Image.asset('images/partnership.png'),
              ),

              Container(
                padding: EdgeInsets.only(top: 20),
                child:Center(child:Text("Add some buddies",style: TextStyle(fontSize: 20,color: Colors.grey,fontWeight: FontWeight.bold),)),
              ),

              Container(
                padding: EdgeInsets.only(top: 30,),
                alignment: Alignment.center,
                child:Center(child:Text("Share your unique link with anyone you'd like to add",textAlign: TextAlign.center
                  ,style: TextStyle(fontSize: 20,color: Colors.grey),)),
              ),
              Container(
                height: 85,
                  padding: EdgeInsets.only(top: 30,left: 20,right: 20),
                child:
                  RaisedButton.icon(
                    onPressed: () {

                    },
                    elevation: 2.0,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                    ),
                    color:Colors.deepOrange,
                    icon: Icon(Icons.share,color: Colors.white,),
                    label: Text("Share Buddy Link",
                      style: TextStyle(
                        fontWeight: FontWeight.w900,color: Colors.white
                      ),
                    ),
                  )
              )

            ],
          ),
    );
  }

}