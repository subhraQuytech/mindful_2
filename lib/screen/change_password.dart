
import 'package:flutter/material.dart';
import 'package:mindful_2/block/change_password_bloc.dart';
import 'package:mindful_2/screen/settings_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ChangePassword extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ChangePasswordState();
  }

}

class _ChangePasswordState extends State<ChangePassword> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  var old_pass = TextEditingController();
  var new_pass = TextEditingController();
  var confirm_pass = TextEditingController();


  bool _isObscure = true;
  bool _isObscureNew_password = true;
  bool _isObscureConfirm_password = true;



  void addChangePasswordListener() {
    changePasswordBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {


          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SettingsPage()));

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }



  @override
  void initState() {
    addChangePasswordListener();
    super.initState();
  }

  @override
  void dispose() {
    addChangePasswordListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [

          ListTile(
            title: Center( child:Text("Change Password",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),)),
            leading: InkWell(
                onTap: (){
                 // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Signup()));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,

                )
            ),
            trailing: Icon(Icons.edit, color: Colors.white12,),
          ),
          Padding(padding: EdgeInsets.only(top: 50)),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20),
            child: Text("Old Password", style: TextStyle(
              fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: TextField(

              obscureText: _isObscure,
              controller: old_pass,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    _isObscure
                        ? Icons.visibility
                        : Icons.visibility_off,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscure = !_isObscure;
                    });
                  },
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                ),

              ),
            ),
          ),



          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: Text("New Password", style: TextStyle(
                fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: TextField(
              controller: new_pass,
              obscureText: _isObscureNew_password,
            //  controller: passwordController,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    _isObscureNew_password
                        ? Icons.visibility
                        : Icons.visibility_off,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscureNew_password = !_isObscureNew_password;
                    });
                  },
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                ),

              ),
            ),
          ),


          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: Text("Confirm Password", style: TextStyle(
                fontSize: 15,fontWeight: FontWeight.bold,color: Colors.grey
            ),
            ),
          ),
          Container(
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: TextField(

              controller: confirm_pass,
              obscureText: _isObscureConfirm_password,
 //             controller: passwordController,
              decoration: InputDecoration(

                suffixIcon: IconButton(
                  icon: Icon(
                    _isObscureConfirm_password
                        ? Icons.visibility
                        : Icons.visibility_off,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscureConfirm_password = !_isObscureConfirm_password;
                    });
                  },
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),

                ),

              ),
            ),
          ),
          SizedBox(height: 60,),
          Container(
            height: 65,
            padding:EdgeInsets.only(left: 20,right: 20,top: 10),
            child: RaisedButton(
              textColor: Colors.white,
              color: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(6.0)
              ),
              onPressed: (){
                _counter = _prefs.then((SharedPreferences prefs) {
                  changePasswordBloc.changePassword(old_pass.text, new_pass.text, confirm_pass.text, prefs.getString("token").toString());
                  return 0;
                });
              },
              child: Text("Confirm"),
            ),
          )

        ],
      ),
    );
  }
}