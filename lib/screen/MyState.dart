import 'package:flutter/material.dart';
import 'package:mindful_2/screen/settings_page.dart';

class MyState extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyStateState();
  }
}

class _MyStateState extends State<MyState> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ListTile(
            title: Center(
                child: Text(
              "My State",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            )),
            leading: InkWell(
                onTap: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SettingsPage()));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                )),
            trailing: Icon(
              Icons.edit,
              color: Colors.white12,
            ),
          ),
          Divider(
            color: Colors.black,
            height: 0,
            thickness: 1,
            indent: 0,
            endIndent: 0,
          ),

          Container(
            child: ListTile(
              title: Text("Current run streak",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                  )),
              subtitle: Text("5 days"),
              tileColor: Colors.white,
              trailing: Icon(
                Icons.arrow_forward_ios_sharp,
                color: Colors.grey,
              ),
            ),
          ),
          Divider(
            color: Colors.black,
            height: 0,
            thickness: 1,
            indent: 0,
            endIndent: 0,
          ),
          ListTile(
            title: Text("Today's Stats",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                )),
            trailing: Icon(
              Icons.share,
              color: Colors.grey,
              size: 30,
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(child:Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.purple[100],
                    borderRadius: BorderRadius.all(Radius.circular(10.0))
                  ),
                  //color: Colors.black26,
                  child: Column(
                    children: [
                      Text(
                        "O minutes",
                        style:
                            TextStyle(color: Colors.deepOrange, fontSize: 20),
                      ),
                     Image.asset('images/partnership.png',width: 100,height: 100,),
                      Text("Total Time",  style:
                      TextStyle(color: Colors.black, fontSize: 18)),
                      Text("Meditation",  style:
                      TextStyle(color: Colors.black, fontSize: 18))

                    ],
                  ),
                )),
                Container(width: 20,),
                Expanded(child: Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.purple[100],
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),

                 // color: Colors.black26,
                  child: Column(

                    children: [
                      Text(
                        "O minutes",
                        style:
                        TextStyle(color: Colors.lightBlueAccent, fontSize: 20),
                      ),
                      Image.asset('images/partnership.png',width: 100,height: 100,),
                      Text("Session",  style:
                      TextStyle(color: Colors.black, fontSize: 18)),
                      Text("Completed",  style:
                      TextStyle(color: Colors.black, fontSize: 18))

                    ],
                  ),
                ))
              ],
            ),
          ),

          Container(
            color: Colors.purple[100],
            padding: EdgeInsets.all(15),
            margin: EdgeInsets.all(10),
            child: ListTile(

              leading:/*Image.network( 'https://via.placeholder.com/150',
                width: 100,
                height: 100,
                fit: BoxFit.cover,),*/ CircleAvatar(
                radius: 25.0,

                backgroundImage: NetworkImage("https://picsum.photos/250?image=9",),
                backgroundColor: Colors.transparent,
              ),
              title: Text('0 minutes',style: TextStyle(color: Colors.lightBlueAccent,fontSize: 20),),
              subtitle: Text("Average Duration",style: TextStyle(fontSize: 20)),
          onTap: (){

          },
            ),


          )

        ],
      ),
    );
  }
}
