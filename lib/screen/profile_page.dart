import 'package:flutter/material.dart';
import 'package:mindful_2/block/change_buddy_link_bloc.dart';
import 'package:mindful_2/block/profile_details_bloc.dart';
import 'package:mindful_2/block/user_bloc.dart';
import 'package:mindful_2/model/profile_details_model.dart';
import 'package:mindful_2/model/user_model.dart';
import 'package:mindful_2/screen/edit_profile_page.dart';
import 'package:mindful_2/screen/settings_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  var buddy_link_txt = "";


  void addProfileDetailsListener() {
    profileDetailsBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {

          setState(() {
            buddy_link_txt = value.data!.buddyLink.toString();
          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }


  void addChangeBuddyLinkListener() {
    changeBuddyLinkBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {

          setState(() {
            buddy_link_txt = value.data!.buddyLink.toString();
          });

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }


  @override
  void initState() {
    super.initState();

    _counter = _prefs.then((SharedPreferences prefs) {
      profileDetailsBloc.getProfileDetails(prefs.getString("token").toString());
      return 0;
    });

    addProfileDetailsListener();
    addChangeBuddyLinkListener();
  }

  @override
  void dispose() {
    addProfileDetailsListener();
    addChangeBuddyLinkListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: StreamBuilder<ProfileDetailsModel>(
          stream: profileDetailsBloc.subject.stream,
          builder: (context, snapshot) {
            if(snapshot.hasData){
              return Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  // background image and bottom contents
                  Column(
                    children: <Widget>[
                      Container(
                        height: 220.0,
                        color: Colors.orange[100],
                        child: Column(
                          children: [
                            SizedBox(
                              height: 30.0,
                            ),
                            ListTile(
                              title: Center(child: Text("My Profile",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)),
                              leading: InkWell(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                  )
                              ),
                              trailing: InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfilePage()));
                                },
                                  child: Icon(Icons.edit, color: Colors.black,)),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 200.0,
                          color: Colors.white,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 200.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Text("Email Address",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                                ],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Text(snapshot.data!.data!.emailId.toString(),style: TextStyle(fontSize: 17.0,color: Colors.black,fontWeight: FontWeight.w500),),
                                ],
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Container(
                                height: 10.0,
                                color: Colors.grey[200],
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Text("Mobile Number",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                                ],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Text(snapshot.data!.data!.mobileNumber.toString(),style: TextStyle(fontSize: 17.0,color: Colors.black,fontWeight: FontWeight.w500),),
                                ],
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Container(
                                height: 10.0,
                                color: Colors.grey[200],
                              ),

                              SizedBox(
                                height: 20.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Text("My Buddy link",style: TextStyle(fontSize: 17.0,color: Colors.black54,fontWeight: FontWeight.w600),),
                                ],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Row(
                                children: [
                                  SizedBox(width: 20.0,),
                                  Container(
                                    width: 300,
                                      child: Text(
                                        buddy_link_txt,
                                        style: TextStyle(
                                            fontSize: 15.0,color: Colors.black,fontWeight: FontWeight.w500
                                        ),
                                      )
                                  ),
                                  SizedBox(width: 30.0,),
                                  InkWell(
                                    onTap: (){
                                      _counter = _prefs.then((SharedPreferences prefs) {
                                        changeBuddyLinkBloc.changeBuddyLink(buddy_link_txt, prefs.getString("token").toString());
                                        return 0;
                                      });
                                    },
                                      child: Icon(
                                        Icons.refresh,color: Colors.grey,
                                      )
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  // Profile image
                  Positioned(
                    top: 140.0, // (background container size) - (circle height / 2)
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage: NetworkImage(
                            snapshot.data!.data!.profileImage.toString(),
                          ),
                          radius: 80.0,
                        ),
                        SizedBox(height: 10.0,),
                        Text( snapshot.data!.data!.name.toString(),style: TextStyle(fontSize: 19.0, ),)
                      ],
                    ),
                  )
                ],
              );
            }else{
              return _errorShimmer();
            }
          }
        ),
      ),
    );
  }

  Widget _errorShimmer() {
    return Center(child: Text("Loading... !!"),);
  }
}
