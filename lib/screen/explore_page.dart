import 'package:flutter/material.dart';
import 'package:mindful_2/screen/profile_page.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({Key? key}) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 200.0,
            color: Colors.orange[50],
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 28.0),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20, bottom: 10),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: getImageAsset()
                        ),
                      ),
                      SizedBox(
                        width: 220,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Icon(Icons.notifications_on_outlined),
                            SizedBox(
                              width: 20.0,
                            ),
                            InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));

                              },
                                child: Icon(Icons.account_circle_outlined)),
                            SizedBox(
                              width: 20.0,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40.0,top: 30.0,right: 30.0,bottom: 30.0),
                  child: Container(
                    height: 50,
                    color: Colors.white,
                    child: TextField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 5.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 5.0),
                        ),
                        hintText: 'Search...',
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          DefaultTabController(
            initialIndex: 0,
            length: 3,

            child: TabBar(
              labelColor: Colors.black,
              indicatorColor: Colors.redAccent,
              labelStyle: TextStyle(fontSize: 17.0,fontWeight: FontWeight.bold),
              labelPadding: EdgeInsets.only(top: 0.0),
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(
                  text: 'English',
                ),
                //child: Image.asset('images/android.png'),

                Tab(
                  text: 'Hindi',
                ),
                Tab(
                  text: 'Punjabi',
                ),
              ],
            ),
          ),


        ],
      ),
    );
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 80,
      height: 80,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 0),
    );
  }
}
