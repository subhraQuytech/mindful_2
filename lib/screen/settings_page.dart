import 'package:flutter/material.dart';
import 'package:mindful_2/block/logout_bloc.dart';
import 'package:mindful_2/screen/Buddies.dart';
import 'package:mindful_2/screen/MyState.dart';
import 'package:mindful_2/screen/change_password.dart';
import 'package:mindful_2/screen/contact_us_page.dart';
import 'package:mindful_2/screen/notification_page.dart';
import 'package:mindful_2/screen/privacy_policy_page.dart';
import 'package:mindful_2/screen/profile_page.dart';
import 'package:mindful_2/screen/sign_in.dart';
import 'package:mindful_2/screen/terms_conditions_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('isLoggedIn', false);
    prefs.setString("name", "");
    prefs.setString("email", "");
    prefs.setString("token", "");
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignIn()));
  }


  void addLogoutListener() {
    logoutBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {
          logout();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }

  @override
  void initState() {
    addLogoutListener();
    super.initState();
  }

  @override
  void dispose() {
    addLogoutListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 60.0,
            color: Colors.orange[50],
            child: Padding(
              padding: const EdgeInsets.only(top: 0.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20, bottom: 10),
                    child: Container(
                      height: 40,
                      width: 40,
                      child: getImageAsset()
                    ),
                  ),
                  SizedBox(
                    width: 220,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(Icons.notifications_on_outlined),
                        SizedBox(
                          width: 20.0,
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                          },
                            child: Icon(
                                Icons.account_circle_outlined
                            )
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => Buddies()));
            },
            child: ListTile(
              title: Text("Buddies"),
              leading: Icon(Icons.timelapse_outlined, color: Colors.purple[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => MyState()));

            },
            child: ListTile(
              title: Text("MyStars"),
              leading: Icon(Icons.point_of_sale,color: Colors.green[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),ListTile(
            title: Text("Downloads"),
            leading: Icon(Icons.download,color: Colors.blue[200],),
            trailing: Icon(Icons.arrow_forward_ios),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword()));

            },
            child: ListTile(
              title: Text("Change Password"),
              leading: Icon(Icons.password_outlined,color: Colors.red[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationPage()));

            },
            child: ListTile(
              title: Text("Notification"),
              leading: Icon(Icons.notifications_active, color: Colors.yellow[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          ListTile(
            title: Text("Invite a friend"),
            leading: Icon(Icons.mobile_friendly,color: Colors.pink[200],),
            trailing: Icon(Icons.arrow_forward_ios),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => ContactUsPage()));
            },
            child: ListTile(
              title: Text("Contact Us"),
              leading: Icon(Icons.contact_mail_outlined,color: Colors.brown[300],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => TermsConditionsPage()));

            },
            child: ListTile(
              title: Text("Terms and Condition"),
              leading: Icon(Icons.construction_rounded,color: Colors.orange[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => PrivacyPolicyPage()));

            },
            child: ListTile(
              title: Text("Privacy Policy"),
              leading: Icon(Icons.policy,color: Colors.deepPurpleAccent[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
          InkWell(
            onTap: (){
              _counter = _prefs.then((SharedPreferences prefs) {

                logoutBloc.logout(prefs.getString("token").toString());

                return 0;

              });
              },
            child: ListTile(
              title: Text("Logout"),
              leading: Icon(Icons.logout,color: Colors.blue[200],),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              height: 1,
              color: Colors.grey[400],
            ),
          ),
        ],
      ),
    );
  }


  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 80,
      height: 80,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 0),
    );
  }
}
