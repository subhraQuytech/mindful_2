import 'package:flutter/material.dart';
import 'package:mindful_2/block/sign_in_bloc.dart';
import 'package:mindful_2/screen/forgot_password_page.dart';
import 'package:mindful_2/screen/home_screen.dart';
import 'package:mindful_2/screen/sign_up.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignInState();
  }
}

class _SignInState extends State<SignIn>{
  var email_txt = TextEditingController();
  var password_txt = TextEditingController();
  bool _isObscure = true;

  void login(String email, String password){
    signinBloc.login(email, password, "12345", "token", "1");
  }

  void addLoginListener() {
    signinBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {
          setState(() async {

            final prefs = await SharedPreferences.getInstance();
            prefs.setBool('isLoggedIn', true);
            prefs.setString("name", signinBloc.subject.stream.value.data!.name.toString());
            prefs.setString("email", signinBloc.subject.stream.value.data!.emailId.toString());
            prefs.setString("token", signinBloc.subject.stream.value.data!.authToken.toString());

            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));

          });
        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }

  @override
  void initState() {
    super.initState();

    addLoginListener();
  }

  @override
  void dispose() {
    addLoginListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            getImageAsset(),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 10),
              child: Text(
                "Sign In",
                style: TextStyle(fontSize: 25, color: Colors.black),
              ),
            ),
            Container(padding: EdgeInsets.only(top: 30),),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "Email/Mobile Number",
                style: TextStyle(
                  fontSize: 15,
                ),
              ),

            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: email_txt,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3),
                  ),

                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "Password",
                style: TextStyle(
                  fontSize: 15,
                ),
              ),

            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(

                obscureText: _isObscure,
                controller: password_txt,
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: Icon(
                      _isObscure
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                    onPressed: () {
                      setState(() {
                        _isObscure = !_isObscure;
                      });
                    },
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),

                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomRight,
                child: FlatButton(onPressed: (){},
                textColor: Colors.grey,
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPasswordPage()));
                  },
                  child: Text("Forgot Password?", style: TextStyle(
                      decoration: TextDecoration.underline),),
                )
            )),
            Container(
              height: 50,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.deepOrange,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                  child: Text('Sign In'),
                  onPressed: () {
                    //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                    login(email_txt.text, password_txt.text);
                  },
                )),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                  child: Row(
                    children: [
                      Expanded(child: const Divider(
                        color: Colors.black,
                        height: 25,
                        thickness: 1,
                        indent: 5,
                        endIndent: 5,
                      )
                      ),
                      Container(padding: EdgeInsets.only(left: 10),),
                      Text("OR"),
                      Container(padding: EdgeInsets.only(right: 10),),
                      Expanded(child: const Divider(
                        color: Colors.black,
                        height: 25,
                        thickness: 1,
                        indent: 5,
                        endIndent: 5,
                      )
                      ),
                    ],
                  ),

                ),
            Container(

              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: Row(
                children: [
                  Expanded(child:
               RaisedButton(
              textColor: Colors.white,
                color: Colors.deepPurple,
                padding: EdgeInsets.all(15),
                child: Text('Facebook'),
                 shape: RoundedRectangleBorder(
                   borderRadius: new BorderRadius.circular(80.0),
                 ),
                onPressed: () {
                  //print(nameController.text);
                  //print(passwordController.text);
                },
              )
                  ),
                  Container(width: 30,),
                  Expanded(child:
                  RaisedButton(
                    textColor: Colors.white,
                    color: Colors.red,
                    padding: EdgeInsets.all(15),
                    child: Text('Google'),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(80.0),
                    ),
                    onPressed: () {
                      //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
                    },
                  )
                  ),
                ],
              ),
            ),

            Container(padding: EdgeInsets.only(top: 30),),
            Container(
            padding: EdgeInsets.only(left: 95),
              child: Row(children: [
                 Text("Dont't have an account?",style: TextStyle(fontSize: 15,color: Colors.black),),
                InkWell(
                  onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Signup()));
                  },
                    child: Text("SignIn",style: TextStyle(color: Colors.deepOrange,fontSize: 15,),
                    )
                )
              ],),
            )


          ],
        ),
      ),
    );
  }
  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 150,
      height: 150,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 60),
    );
  }
}
