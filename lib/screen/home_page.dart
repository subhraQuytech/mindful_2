import 'package:flutter/material.dart';
import 'package:mindful_2/block/dashboard_meditation_bloc.dart';
import 'package:mindful_2/block/my_meditation_bloc.dart';
import 'package:mindful_2/block/recommand_meditation_bloc.dart';
import 'package:mindful_2/model/my_maditations_model.dart';
import 'package:mindful_2/model/recommand_meditation_model.dart';
import 'package:mindful_2/screen/notification_page.dart';
import 'package:mindful_2/screen/profile_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  var limit = 10;
  var offset = 0;
  var dashBoardImageUrl = "https://images.unsplash.com/photo-1489908990827-08a75c580832?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80";
  var dashboardMeditationTitle;
  var dashboardMeditationDuration = "20";

  void addDashboardMeditation() {
    dashboardMeditationBloc.subject.stream.listen((value) {
      if(value.error == null){
        if (value.status == true) {

          setState(() {
            dashBoardImageUrl = value.data!.courseMeditation!.courseCoverImage.toString();
            dashboardMeditationTitle = value.data!.courseMeditation!.courseMedName.toString();
            dashboardMeditationDuration = value.data!.courseMeditation!.medAudio!.totalDuration.toString();
          });

        } else {
          setState(() {

          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(value.message.toString()),
          ));
        }
      } else {
        setState(() {
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(value.error.toString()),
        ));
      }

    });
  }

  @override
  void initState() {

    _counter = _prefs.then((SharedPreferences prefs) {
      dashboardMeditationBloc.getDashboardMeditation(prefs.getString("token").toString());
      myMeditationBloc.getMyMeditation(limit.toString(), offset.toString(), prefs.getString("token").toString());
      recommandMeditationBloc.getRecommandMeditation(limit.toString(), offset.toString(), prefs.getString("token").toString());
      return 0;
    });

    //addDashboardMeditation();

    super.initState();
  }

  @override
  void dispose() {
    //addDashboardMeditation();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:28.0),
              child: Container(
                height: 60.0,
                color: Colors.orange[50],
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20, bottom: 10),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: getImageAsset()
                        ),
                      ),
                      SizedBox(
                        width: 220,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                                child: Icon(Icons.notifications_on_outlined),
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationPage()));
                              },
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                              },
                                child: Icon(Icons.account_circle_outlined)),
                            SizedBox(
                              width: 20.0,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Expanded(
                child: Container(
                    child: Image.network(
                        dashBoardImageUrl
                    )
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Container(
                height: 2,
                color: Colors.black,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10.0),
              child: Text("Let us Meditate!",style: TextStyle(
                fontSize: 20.0,
                color: Colors.black
              ),),

            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0,right: 20,top: 10.0),
              child: Text(dashboardMeditationDuration+" SEC.COURSE",style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0,right: 20,top: 20.0),
              child: Text("Recommendation for you",style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 20.0),
                height: 200.0,
                child: StreamBuilder<RecommandMeditationModel>(
                    stream: recommandMeditationBloc.subject.stream,
                    builder: (context, snapshot) {
                      if(snapshot.hasData){
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data!.data!.courseMeditation!.length,
                            scrollDirection:Axis.horizontal,
                            itemBuilder: (context, index){
                              CourseMeditation2 _course = snapshot.data!.data!.courseMeditation![index];
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      width: 200.0,
                                      height: 100.0,
                                      child: Image.network(
                                          _course.courseImage.toString()
                                      )
                                  ),
                                  SizedBox(height: 10.0,),
                                  Text(_course.courseMedName.toString(),style: TextStyle(fontSize: 18.0),),
                                  Text(_course.medAudio!.listeningDuration.toString()+"Sec.COURSE",style: TextStyle(fontSize: 14.0),)
                                ],
                              );
                            }
                        );

                      }else{
                        return _loadingShimmer();
                      }

                    }
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0,right: 20,top: 0.0),
              child: Text("My meditation",style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 20.0),
                height: 200.0,
                child: StreamBuilder<MyMeditationModel>(
                  stream: myMeditationBloc.subject.stream,
                  builder: (context, snapshot) {
                    if(snapshot.hasData){
                    return ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data!.data!.courseMeditation!.length,
                        scrollDirection:Axis.horizontal,
                        itemBuilder: (context, index){
                          CourseMeditation _course = snapshot.data!.data!.courseMeditation![index];
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: 200.0,
                                  height: 100.0,
                                  child: Image.network(
                                    _course.courseImage.toString()
                                  )
                              ),
                              SizedBox(height: 10.0,),
                              Text(_course.courseMedName.toString(),style: TextStyle(fontSize: 18.0),),
                              Text(_course.medAudio!.listeningDuration.toString()+"Sec.COURSE",style: TextStyle(fontSize: 14.0),)
                            ],
                          );
                        }
                    );

                    }else{
                      return _loadingShimmer();
                    }

                  }
                ),
              ),
            ),

          ],
        ),
      )
    );
  }

  Widget _loadingShimmer() {
    return Center(child: Text("Loading..."),);
  }


  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 80,
      height: 80,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 0),
    );
  }
}
