import 'package:flutter/material.dart';
import 'package:mindful_2/block/sleep_course_bloc.dart';
import 'package:mindful_2/model/sleep_course_model.dart';
import 'package:mindful_2/screen/profile_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SleepPage extends StatefulWidget {
  const SleepPage({Key? key}) : super(key: key);

  @override
  _SleepPageState createState() => _SleepPageState();
}

class _SleepPageState extends State<SleepPage> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;

  var limit = 10;
  var offset = 0;

  @override
  void initState() {

    _counter = _prefs.then((SharedPreferences prefs) {
      sleepCourseBloc.getSleepCourse(limit.toString(), offset.toString(), prefs.getString("token").toString());
      return 0;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body:ListView(
        children: [
          Container(
            height: 60.0,
            color: Colors.blue[900],
            child: Padding(
              padding: const EdgeInsets.only(top: 0.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20, bottom: 10),
                    child: Container(
                      height: 40,
                      width: 40,
                      child: getImageAsset(),
                    ),
                  ),
                  SizedBox(
                    width: 220,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(Icons.notifications_on_outlined,color: Colors.white,),
                        SizedBox(
                          width: 20.0,
                        ),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));

                          },
                            child: Icon(Icons.account_circle_outlined,color: Colors.white,)),
                        SizedBox(
                          width: 20.0,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 20.0),
              height: 200.0,
              child: StreamBuilder<SleepCourseModel>(
                  stream: sleepCourseBloc.subject.stream,
                  builder: (context, snapshot) {
                    if(snapshot.hasData){
                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: snapshot.data!.data!.courseMeditation!.length,
                          scrollDirection:Axis.horizontal,
                          itemBuilder: (context, index){
                            CourseMeditation _course = snapshot.data!.data!.courseMeditation![index];
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                    width: 200.0,
                                    height: 100.0,
                                    child: Image.network(
                                        _course.courseImage.toString()
                                    )
                                ),
                                SizedBox(height: 10.0,),
                                Text(_course.courseMedName.toString(),style: TextStyle(fontSize: 18.0, color: Colors.white),),
                                Text(_course.medAudio!.listeningDuration.toString()+"Sec.COURSE",style: TextStyle(fontSize: 14.0, color: Colors.white),)
                              ],
                            );
                          }
                      );

                    }else{
                      return _loadingShimmer();
                    }

                  }
              ),
            ),
          ),
        ],
      )
    );
  }


  Widget _loadingShimmer() {
    return Center(child: Text("Loading..."),);
  }


  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/minful_icon_new.png');
    Image image = Image(
      image: assetImage,
      width: 80,
      height: 80,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 0),
    );
  }
}
