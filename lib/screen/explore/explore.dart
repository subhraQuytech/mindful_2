import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mindful_2/screen/explore/English.dart';
import 'package:mindful_2/screen/explore/Hindi.dart';
import 'package:mindful_2/screen/explore/Punjabi.dart';
import 'package:mindful_2/screen/notification_page.dart';
import 'package:mindful_2/screen/profile_page.dart';
class Explore extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ExploreState();
  }
}

class _ExploreState extends State<Explore> with SingleTickerProviderStateMixin{
TabController? _tabController;



@override
  void initState() {
   _tabController=new TabController(length: 3, vsync: this);

   super.initState();
  }
  /* TabController? _tabController;
  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync:this);
    super.initState();
  }*/
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.orange[50]));

    return Scaffold(
      body: Container(
        child: Column(
         // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 250,
              width: MediaQuery.of(context).size.height / 2,
             padding: EdgeInsets.only(top: 30),
             decoration: BoxDecoration(
               color: Colors.orange[50],
               borderRadius: BorderRadius.only(
                 bottomLeft: Radius.circular(30),
                 bottomRight: Radius.circular(30),
               ),
             ),
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 Row(
                   mainAxisAlignment: MainAxisAlignment.start,
                   children: [
                     InkWell(
                         onTap: (){
                           //  Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));

                         },
                       child: Image.asset("images/minful_icon_new.png",width: 80,height: 80,),
                     ),
                    /* Icon(Icons.notifications_on_outlined),
                     SizedBox(
                       width: 20.0,
                     ),*/
                     SizedBox(
                       width: 220.0,
                     ),
                     InkWell(
                         onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationPage()));
                         },
                         child: Icon(Icons.notifications_active_outlined,color: Colors.grey,size: 30,)
                     ),
                     /*SizedBox(
                       width: 20.0,
                     ),
                    */
                     SizedBox(
                       width: 20.0,
                     ),
                     InkWell(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));

                         },
                         child: Icon(Icons.account_circle_outlined,color: Colors.grey,size: 30,)
                     ),
                   ],
                 ),
                 /*ListTile(
                   leading: Icon(Icons.account_circle,size: 30,),
                   onTap: () {
                     setState(() {

                     });
                   },
                 ),*/
                 Padding(
                   padding: const EdgeInsets.only(left: 40.0,top: 45.0,right: 40.0),
                   child: Container(
                     height: 50,
                     decoration:BoxDecoration(

                       color: Colors.white,
                       borderRadius: BorderRadius.all(Radius.circular(23))
                     ),
                     padding: EdgeInsets.only(left: 10,right: 10),


                     child: TextFormField(
                       decoration: InputDecoration(
                         border: InputBorder.none,
                         hintText: 'Search...',
                         hintStyle: TextStyle(
                           color: Colors.grey
                         ),
                         icon: Icon(Icons.search,color: Colors.grey,)
                       ),
                     ),
                   ),
                 )

               ],
             ),
             /* child: Center(
                child: Text(
                  "Tabbar with out Appbar",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),

              ),*/
             // color: Colors.blue,
            ),
            TabBar(
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.black,
              indicatorColor:Colors.white60,
              tabs: [
                Tab(
                  text: 'English',
                ),
                Tab(
                  text: 'Hindi',
                ),
                Tab(
                  text: 'Punjabi',
                )
              ],
              controller: _tabController,
              indicatorSize: TabBarIndicatorSize.tab,
            ),
            Expanded(
              child: TabBarView(
                children: [
                  English(),
                  Hindi(),
                  Punjabi()

                ],
                controller: _tabController,
              ),
            ),
          ],
        ),
      ),
    );

  }

}