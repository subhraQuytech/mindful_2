import 'package:flutter/material.dart';

class Hindi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 20.0),
          height: 200.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 200.0,
                      child: Image.network(
                          'https://media.istockphoto.com/photos/woman-wearing-headphones-sitting-at-home-in-the-bed-in-lotus-position-picture-id1318457433?s=2048x2048'
                      )
                  ),
                  SizedBox(height: 10.0,),
                  Text("हिन्दी",style: TextStyle(fontSize: 18.0,color: Colors.black),),
                  Text("0-21 स्थिति या दशा में ",style: TextStyle(fontSize: 14.0,color: Colors.black),)
                ],
              ),
              SizedBox(
                width: 10.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 200.0,
                      child: Image.network(
                          'https://media.istockphoto.com/photos/african-american-woman-day-dreaming-in-public-park-on-beautiful-day-picture-id1289286426?s=2048x2048'
                      )
                  ),
                  SizedBox(height: 10.0,),
                  Text("हिन्दी",style: TextStyle(fontSize: 18.0,color: Colors.black),),
                  Text("0-21 स्थिति या दशा में परिवर्तन",style: TextStyle(fontSize: 14.0,color: Colors.black),)
                ],
              ),
              SizedBox(
                width: 10.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 200.0,
                      child: Image.network(
                          'https://media.istockphoto.com/photos/woman-wearing-headphones-sitting-at-home-in-the-bed-in-lotus-position-picture-id1318457433?s=2048x2048'
                      )
                  ),
                  SizedBox(height: 10.0,),
                  Text("हिन्दी",style: TextStyle(fontSize: 18.0,color: Colors.black),),
                  Text("0-21 स्थिति या दशा में परिवर्तन",style: TextStyle(fontSize: 14.0,color: Colors.black),)
                ],
              ),
            ],
          ),
        ),
      ),

    );
  }
}