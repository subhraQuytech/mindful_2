import 'package:mindful_2/model/my_maditations_model.dart';
import 'package:mindful_2/repository/my_meditation_repository.dart';
import 'package:rxdart/rxdart.dart';

class MyMeditationBloc{
  final MyMeditationRepo _myMeditationRepo = MyMeditationRepo();
  final BehaviorSubject<MyMeditationModel> _behaviourSubject = BehaviorSubject<MyMeditationModel>();

  getMyMeditation(String limit, String offset, String token) async{
    MyMeditationModel response = await _myMeditationRepo.getMyMeditation(limit,offset,token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<MyMeditationModel> get subject => _behaviourSubject;
}

final myMeditationBloc = MyMeditationBloc();