import 'package:mindful_2/model/privacy_policy_Model.dart';
import 'package:mindful_2/repository/privacy_policy_repository.dart';
import 'package:rxdart/rxdart.dart';

class PrivacyPolicyBloc{
  final PrivacyPolicyRepo _privacyPolicyRepo = PrivacyPolicyRepo();
  final BehaviorSubject<PrivacyPolicyModel> _behaviourSubject = BehaviorSubject<PrivacyPolicyModel>();

  getPrivacyPolicy() async{
    PrivacyPolicyModel? response = await _privacyPolicyRepo.getPrivacyPolicy();
    _behaviourSubject.sink.add(response!);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<PrivacyPolicyModel> get subject => _behaviourSubject;
}

final privacyPolicyBloc = PrivacyPolicyBloc();