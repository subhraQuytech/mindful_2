import 'package:mindful_2/model/recommand_meditation_model.dart';
import 'package:mindful_2/repository/recommand_meditation_repo.dart';
import 'package:rxdart/rxdart.dart';

class RecommandMeditationBloc{
  final RecommandMeditationRepo _myMeditationRepo = RecommandMeditationRepo();
  final BehaviorSubject<RecommandMeditationModel> _behaviourSubject = BehaviorSubject<RecommandMeditationModel>();

  getRecommandMeditation(String limit, String offset, String token) async{
    RecommandMeditationModel response = await _myMeditationRepo.getRecommandMeditation(limit,offset,token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<RecommandMeditationModel> get subject => _behaviourSubject;
}

final recommandMeditationBloc = RecommandMeditationBloc();