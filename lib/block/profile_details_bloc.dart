import 'package:mindful_2/model/profile_details_model.dart';
import 'package:mindful_2/repository/profile_details_repository.dart';
import 'package:rxdart/rxdart.dart';

class ProfileDetailsBloc{
  final ProfileDetailsRepo _profileDetailsRepo = ProfileDetailsRepo();
  final BehaviorSubject<ProfileDetailsModel> _behaviourSubject = BehaviorSubject<ProfileDetailsModel>();

  getProfileDetails(String token) async{
    ProfileDetailsModel response = await _profileDetailsRepo.getProfileDetails(token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<ProfileDetailsModel> get subject => _behaviourSubject;
}
final profileDetailsBloc = ProfileDetailsBloc();