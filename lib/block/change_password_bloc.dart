import 'package:mindful_2/model/change_password_model.dart';
import 'package:mindful_2/repository/change_password_repository.dart';
import 'package:rxdart/rxdart.dart';

class ChangePasswordBloc{
  final ChangePasswordRepo _contactUsRepo = ChangePasswordRepo();
  final BehaviorSubject<ChangePasswordModel> _behaviourSubject = BehaviorSubject<ChangePasswordModel>();

  changePassword(String old_pass, String new_pass, String confirm_pass, String token) async{
    ChangePasswordModel response = await _contactUsRepo.changePassword(old_pass,new_pass,confirm_pass,token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<ChangePasswordModel> get subject => _behaviourSubject;
}
final changePasswordBloc = ChangePasswordBloc();