import 'package:mindful_2/model/logout_model.dart';
import 'package:mindful_2/repository/logout_repository.dart';
import 'package:rxdart/rxdart.dart';

class LogoutBloc{
  final LogoutRepo _logoutRepo = LogoutRepo();
  final BehaviorSubject<LogoutModel> _behaviourSubject = BehaviorSubject<LogoutModel>();

  logout(String token) async{
    LogoutModel? response = await _logoutRepo.logout(token);
    _behaviourSubject.sink.add(response!);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<LogoutModel> get subject => _behaviourSubject;
}

final logoutBloc = LogoutBloc();