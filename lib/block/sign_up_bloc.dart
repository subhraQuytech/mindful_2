import 'package:mindful_2/model/signup_model.dart';
import 'package:mindful_2/repository/sign_up_repository.dart';
import 'package:rxdart/rxdart.dart';

class SignUpBloc{
  final SignUpRepo _signUpRepo = SignUpRepo();
  final BehaviorSubject<SignUpModel> _behaviourSubject = BehaviorSubject<SignUpModel>();

  signUp(String name, String email_id, String country_code, String mobile_number, String device_id, String device_token,String password,String confirm_password, String device_type_id) async{
    SignUpModel response = await _signUpRepo.signUp(name,email_id,country_code,mobile_number,device_id,device_token,password,confirm_password,device_type_id);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<SignUpModel> get subject => _behaviourSubject;
}
final signUpBloc = SignUpBloc();