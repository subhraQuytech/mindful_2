import 'package:mindful_2/model/user_model.dart';
import 'package:mindful_2/repository/user_repository.dart';
import 'package:rxdart/rxdart.dart';

class ProjectsBloc{
  final UsersRepo _projectsRepo = UsersRepo();
  final BehaviorSubject<UserModel> _behaviourSubject = BehaviorSubject<UserModel>();

  getUsers() async{
    UserModel? response = await _projectsRepo.getUsers();
    _behaviourSubject.sink.add(response!);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<UserModel> get subject => _behaviourSubject;
}

final usersBloc = ProjectsBloc();