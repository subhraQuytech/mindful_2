import 'package:mindful_2/model/ContactUsModel.dart';
import 'package:mindful_2/repository/contact_us_repository.dart';
import 'package:rxdart/rxdart.dart';

class ContactUsBloc{
  final ContactUsRepo _contactUsRepo = ContactUsRepo();
  final BehaviorSubject<ContactUsModel> _behaviourSubject = BehaviorSubject<ContactUsModel>();

  contactUs(String name, String email_id, String message, String token) async{
    ContactUsModel response = await _contactUsRepo.contactUs(name,email_id,message, token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<ContactUsModel> get subject => _behaviourSubject;
}
final contactUsBloc = ContactUsBloc();