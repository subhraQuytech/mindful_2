import 'package:mindful_2/model/dashboard_meditation_model.dart';
import 'package:mindful_2/repository/dashboard_meditation_repository.dart';
import 'package:rxdart/rxdart.dart';

class DashboardMeditationBloc{
  final DashboardMeditationRepo _dashboardMeditationRepo = DashboardMeditationRepo();
  final BehaviorSubject<DashBoardMeditationModel> _behaviourSubject = BehaviorSubject<DashBoardMeditationModel>();

  getDashboardMeditation(String token) async{
    DashBoardMeditationModel response = await _dashboardMeditationRepo.getDashboardMeditation(token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<DashBoardMeditationModel> get subject => _behaviourSubject;
}

final dashboardMeditationBloc = DashboardMeditationBloc();