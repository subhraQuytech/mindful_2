import 'package:mindful_2/model/sleep_course_model.dart';
import 'package:mindful_2/repository/sleep_course_repository.dart';
import 'package:rxdart/rxdart.dart';

class SleepCourseBloc{
  final SleepCourseRepo _sleepCourseRepo = SleepCourseRepo();
  final BehaviorSubject<SleepCourseModel> _behaviourSubject = BehaviorSubject<SleepCourseModel>();

  getSleepCourse(String limit, String offset, String token) async{
    SleepCourseModel response = await _sleepCourseRepo.getSleepCourse(limit,offset,token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<SleepCourseModel> get subject => _behaviourSubject;
}

final sleepCourseBloc = SleepCourseBloc();