import 'package:mindful_2/model/sign_in_model.dart';
import 'package:mindful_2/repository/sign_in_repository.dart';
import 'package:rxdart/rxdart.dart';

class SignInBloc{
  final SigninRepo _signinRepo = SigninRepo();
  final BehaviorSubject<SignInModel> _behaviourSubject = BehaviorSubject<SignInModel>();

  login(String email, String password, String deviceId, String deviceToken, String deviceTyeId) async{
    SignInModel response = await _signinRepo.login(email,password,deviceId,deviceToken,deviceTyeId);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<SignInModel> get subject => _behaviourSubject;
}
final signinBloc = SignInBloc();