import 'package:mindful_2/model/terms_and_conditions_Model.dart';
import 'package:mindful_2/repository/terms_conditions_repository.dart';
import 'package:rxdart/rxdart.dart';

class TermsBloc{
  final TermsRepo _termsRepo = TermsRepo();
  final BehaviorSubject<TermsModel> _behaviourSubject = BehaviorSubject<TermsModel>();

  getTerms() async{
    TermsModel? response = await _termsRepo.getTerms();
    _behaviourSubject.sink.add(response!);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<TermsModel> get subject => _behaviourSubject;
}

final termsBloc = TermsBloc();