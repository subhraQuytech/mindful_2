import 'package:mindful_2/model/change_buddy_link.dart';
import 'package:mindful_2/repository/change_buddy_link_repository.dart';
import 'package:rxdart/rxdart.dart';

class ChangeBuddyLinkBloc{
  final ChangeBuddyLinkRepo _changeBuddyLinkRepo = ChangeBuddyLinkRepo();
  final BehaviorSubject<ChangeBuddyLinkModel> _behaviourSubject = BehaviorSubject<ChangeBuddyLinkModel>();

  changeBuddyLink(String buddy_link, String token) async{
    ChangeBuddyLinkModel response = await _changeBuddyLinkRepo.changeBuddyLink(buddy_link, token);
    _behaviourSubject.sink.add(response);
  }

  dispose(){
    _behaviourSubject.close();
  }

  BehaviorSubject<ChangeBuddyLinkModel> get subject => _behaviourSubject;
}
final changeBuddyLinkBloc = ChangeBuddyLinkBloc();