import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/sleep_course_model.dart';

class SleepCourseRepo{
  Future<SleepCourseModel> getSleepCourse(String limit, offset, token) async {
    SleepCourseModel _sleepCourseModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'limit': limit, 'offset': offset};

    try {
      Response response = await _apiClient!.post(ApiLinks.sleep_course, data: params);
      _sleepCourseModel = SleepCourseModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _sleepCourseModel = SleepCourseModel.withError(e.message);
      // throw Exception(e);
    }

    return _sleepCourseModel;
  }
}