import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/change_buddy_link.dart';

class ChangeBuddyLinkRepo{
  Future<ChangeBuddyLinkModel> changeBuddyLink(String buddy_link, token) async {
    ChangeBuddyLinkModel _changeBuddyLinkModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'buddy_link': buddy_link,};

    try {
      Response response = await _apiClient!.post(ApiLinks.change_buddy_link, data: params);
      _changeBuddyLinkModel = ChangeBuddyLinkModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _changeBuddyLinkModel = ChangeBuddyLinkModel.withError(e.message);
      // throw Exception(e);
    }

    return _changeBuddyLinkModel;
  }
}