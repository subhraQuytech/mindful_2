import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/change_password_model.dart';

class ChangePasswordRepo{
  Future<ChangePasswordModel> changePassword(String old_pass, new_pass, confirm_pass, token) async {
    ChangePasswordModel _changePasswordModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'old_password': old_pass, 'new_password': new_pass, 'confirm_password':confirm_pass};

    try {
      Response response = await _apiClient!.post(ApiLinks.change_password, data: params);
      _changePasswordModel = ChangePasswordModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _changePasswordModel = ChangePasswordModel.withError(e.message);
      // throw Exception(e);
    }

    return _changePasswordModel;
  }
}