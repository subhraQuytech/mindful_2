import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/sign_in_model.dart';

class SigninRepo{
  Future<SignInModel> login(String email, password, diviceId, deviceToken, deviceTypeId) async {
    SignInModel _signInModel;
    final _apiClient = ApiClient.http();
    var params = {'email_id': email, 'password': password, 'device_id':diviceId, 'device_token':deviceToken, 'device_type_id':deviceTypeId};

    try {
      Response response = await _apiClient!.post(ApiLinks.sign_in, data: params);
      _signInModel = SignInModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _signInModel = SignInModel.withError(e.message);
      // throw Exception(e);
    }

    return _signInModel;
  }
}