import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/user_model.dart';
class UsersRepo{
  Future<UserModel?> getUsers() async {
    UserModel? _projects;
    final _apiClient = ApiClient.http();
    //var params = {'mobile': mobileNumber};
    try {
      Response response = await _apiClient!.get(ApiLinks.users);
      _projects = UserModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _projects = UserModel.withError(e.message);
      // throw Exception(e);
    }
    return _projects;
  }
}
