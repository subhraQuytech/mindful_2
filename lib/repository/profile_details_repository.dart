import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/profile_details_model.dart';

class ProfileDetailsRepo{
  Future<ProfileDetailsModel> getProfileDetails(String token) async {
    ProfileDetailsModel _profileDetailsModel;
    final _apiClient = ApiClient.httpWithToken(token);
    try {
      Response response = await _apiClient!.get(ApiLinks.profile_details);
      _profileDetailsModel = ProfileDetailsModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _profileDetailsModel = ProfileDetailsModel.withError(e.message);
      // throw Exception(e);
    }

    return _profileDetailsModel;
  }
}