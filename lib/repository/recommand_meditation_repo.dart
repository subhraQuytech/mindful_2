import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/recommand_meditation_model.dart';

class RecommandMeditationRepo{
  Future<RecommandMeditationModel> getRecommandMeditation(String limit, offset, token) async {
    RecommandMeditationModel _myMeditationModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'limit': limit, 'offset': offset};

    try {
      Response response = await _apiClient!.post(ApiLinks.recommand_meditation, data: params);
      _myMeditationModel = RecommandMeditationModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _myMeditationModel = RecommandMeditationModel.withError(e.message);
      // throw Exception(e);
    }

    return _myMeditationModel;
  }
}