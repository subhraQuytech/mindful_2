import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/terms_and_conditions_Model.dart';

class TermsRepo{
  Future<TermsModel> getTerms() async {
    TermsModel _termsModel;
    final _apiClient = ApiClient.http();
    try {
      Response response = await _apiClient!.get(ApiLinks.terms_conditions);
      _termsModel = TermsModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _termsModel = TermsModel.withError(e.message);
      // throw Exception(e);
    }

    return _termsModel;
  }
}