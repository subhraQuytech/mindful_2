import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/privacy_policy_Model.dart';

class PrivacyPolicyRepo{
  Future<PrivacyPolicyModel> getPrivacyPolicy() async {
    PrivacyPolicyModel _signInModel;
    final _apiClient = ApiClient.http();
    try {
      Response response = await _apiClient!.get(ApiLinks.privacy_policy);
      _signInModel = PrivacyPolicyModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _signInModel = PrivacyPolicyModel.withError(e.message);
      // throw Exception(e);
    }

    return _signInModel;
  }
}