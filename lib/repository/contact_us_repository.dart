import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/ContactUsModel.dart';

class ContactUsRepo{
  Future<ContactUsModel> contactUs(String name, email_id, message, token) async {
    ContactUsModel _contactUsModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'name': name, 'email_id': email_id, 'message':message};

    try {
      Response response = await _apiClient!.post(ApiLinks.contact_us, data: params);
      _contactUsModel = ContactUsModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _contactUsModel = ContactUsModel.withError(e.message);
      // throw Exception(e);
    }

    return _contactUsModel;
  }
}