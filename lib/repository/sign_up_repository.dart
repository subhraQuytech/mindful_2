
import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/signup_model.dart';

class SignUpRepo{
  Future<SignUpModel> signUp(String name, email_id, country_code, mobile_number, device_id, device_token, password, confirm_password, device_type_id) async {
    SignUpModel _signUpModel;
    final _apiClient = ApiClient.http();
    var params = {'name': name, 'email_id': email_id, 'country_code':country_code, 'mobile_number':mobile_number, 'device_id':device_id, 'device_token':device_token ,'password':password, 'confirm_password':confirm_password, 'device_type_id':device_type_id};

    try {
      Response response = await _apiClient!.post(ApiLinks.sign_up, data: params);
      _signUpModel = SignUpModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _signUpModel = SignUpModel.withError(e.message);
      // throw Exception(e);
    }

    return _signUpModel;
  }
}