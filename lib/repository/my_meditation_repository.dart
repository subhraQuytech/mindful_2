import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/my_maditations_model.dart';

class MyMeditationRepo{
  Future<MyMeditationModel> getMyMeditation(String limit, offset, token) async {
    MyMeditationModel _myMeditationModel;
    final _apiClient = ApiClient.httpWithToken(token);
    var params = {'limit': limit, 'offset': offset};

    try {
      Response response = await _apiClient!.post(ApiLinks.my_meditation, data: params);
      _myMeditationModel = MyMeditationModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _myMeditationModel = MyMeditationModel.withError(e.message);
      // throw Exception(e);
    }

    return _myMeditationModel;
  }
}