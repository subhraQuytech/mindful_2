import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiClient.dart';
import 'package:mindful_2/api/ApiLinks.dart';
import 'package:mindful_2/model/dashboard_meditation_model.dart';

class DashboardMeditationRepo{
  Future<DashBoardMeditationModel> getDashboardMeditation(String token) async {
    DashBoardMeditationModel _dashBoardMeditationModel;
    final _apiClient = ApiClient.httpWithToken(token);

    try {
      Response response = await _apiClient!.get(ApiLinks.dashboard_meditation);
      _dashBoardMeditationModel = DashBoardMeditationModel.fromJson(response.data);
    } on DioError catch (e){
      // DioError dioError = e;
      // print('THERE IS SOME ERROR IN NETWORK CALL.. ERROR => ${e.error.toString()} and MESSAGE => ${e.message}');
      _dashBoardMeditationModel = DashBoardMeditationModel.withError(e.message);
      // throw Exception(e);
    }

    return _dashBoardMeditationModel;
  }
}