class ApiLinks{
  static const String baseUrl = debugUrl;
  //static const String debugUrl = 'https://gorest.co.in/public/v1/';
  static const String debugUrl = 'http://13.127.138.63/mindful_happiness/mobileapi/';

  static const String users = 'users';

  static const String sign_in = 'sign_in';
  static const String sign_up = 'sign_up';
  static const String profile_details = 'profile_detail';
  static const String privacy_policy = 'privacy_policy';
  static const String terms_conditions = 'terms_condition';
  static const String logout = 'logout';
  static const String contact_us = 'contact_us';
  static const String change_buddy_link = 'change_buddy_link';
  static const String change_password = 'change_password';
  static const String my_meditation = 'get_course_meditation_list';
  static const String recommand_meditation = 'get_recommendations';
  static const String sleep_course = 'get_sleep_course_med';
  static const String dashboard_meditation = 'get_dashboard_meditation';

}