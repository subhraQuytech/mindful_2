import 'dart:io';

import 'package:dio/dio.dart';
import 'package:mindful_2/api/ApiLinks.dart';

class ApiClient{

  static Dio? _dio;
  static Dio? http() {
    if (_dio == null) {
      //print('Creating new instance');
      _dio = new Dio();
      _dio!.options.baseUrl = ApiLinks.baseUrl;
      // _dio.interceptors.add(TokenInterceptor(dio: _dio));
      _dio!.interceptors.add(LogInterceptor(responseBody: true));
    }
    //print('Has instance');
    return _dio;
  }


  static Dio? httpWithToken(String token) {
    if (_dio == null) {
      //print('Creating new instance');
      _dio = new Dio();
      _dio!.options.baseUrl = ApiLinks.baseUrl;
      _dio!.options.headers["Authorization"] = token;
      // _dio.interceptors.add(TokenInterceptor(dio: _dio));
      _dio!.interceptors.add(LogInterceptor(responseBody: true));
    }
    //print('Has instance');
    return _dio;
  }

  static dispose() {
    if (_dio != null) _dio!.close();
  }
}