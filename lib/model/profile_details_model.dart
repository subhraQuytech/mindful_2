class ProfileDetailsModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  ProfileDetailsModel({this.status, this.message, this.data, this.code});
  ProfileDetailsModel.withError(String err): error = err;

  ProfileDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  String? userId;
  String? name;
  String? emailId;
  String? countryCode;
  String? mobileNumber;
  String? profileImage;
  String? buddyLink;
  bool? subscription;

  Data(
      {this.userId,
        this.name,
        this.emailId,
        this.countryCode,
        this.mobileNumber,
        this.profileImage,
        this.buddyLink,
        this.subscription});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    name = json['name'];
    emailId = json['email_id'];
    countryCode = json['country_code'];
    mobileNumber = json['mobile_number'];
    profileImage = json['profile_image'];
    buddyLink = json['buddy_link'];
    subscription = json['subscription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['email_id'] = this.emailId;
    data['country_code'] = this.countryCode;
    data['mobile_number'] = this.mobileNumber;
    data['profile_image'] = this.profileImage;
    data['buddy_link'] = this.buddyLink;
    data['subscription'] = this.subscription;
    return data;
  }
}