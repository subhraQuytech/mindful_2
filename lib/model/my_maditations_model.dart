class MyMeditationModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  MyMeditationModel({this.status, this.message, this.data, this.code});
  MyMeditationModel.withError(String err): error=err;

  MyMeditationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<CourseMeditation>? courseMeditation;
  TotalPages? totalPages;

  Data({this.courseMeditation, this.totalPages});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['course_meditation'] != null) {
      courseMeditation = <CourseMeditation>[];
      json['course_meditation'].forEach((v) {
        courseMeditation!.add(new CourseMeditation.fromJson(v));
      });
    }
    totalPages = json['total_pages'] != null
        ? new TotalPages.fromJson(json['total_pages'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.courseMeditation != null) {
      data['course_meditation'] =
          this.courseMeditation!.map((v) => v.toJson()).toList();
    }
    if (this.totalPages != null) {
      data['total_pages'] = this.totalPages!.toJson();
    }
    return data;
  }
}

class CourseMeditation {
  String? courseMeditationId;
  String? courseTypeId;
  String? courseAudioId;
  String? courseActivityId;
  String? courseMedName;
  String? courseMedDescription;
  String? courseCoverImage;
  String? courseImage;
  String? createdAt;
  String? moodId;
  String? moodTitle;
  int? isSleep;
  MedAudio? medAudio;

  CourseMeditation(
      {this.courseMeditationId,
        this.courseTypeId,
        this.courseAudioId,
        this.courseActivityId,
        this.courseMedName,
        this.courseMedDescription,
        this.courseCoverImage,
        this.courseImage,
        this.createdAt,
        this.moodId,
        this.moodTitle,
        this.isSleep,
        this.medAudio});

  CourseMeditation.fromJson(Map<String, dynamic> json) {
    courseMeditationId = json['course_meditation_id'];
    courseTypeId = json['course_type_id'];
    courseAudioId = json['course_audio_id'];
    courseActivityId = json['course_activity_id'];
    courseMedName = json['course_med_name'];
    courseMedDescription = json['course_med_description'];
    courseCoverImage = json['course_cover_image'];
    courseImage = json['course_image'];
    createdAt = json['created_at'];
    moodId = json['mood_id'];
    moodTitle = json['mood_title'];
    isSleep = json['isSleep'];
    medAudio = json['med_audio'] != null
        ? new MedAudio.fromJson(json['med_audio'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['course_meditation_id'] = this.courseMeditationId;
    data['course_type_id'] = this.courseTypeId;
    data['course_audio_id'] = this.courseAudioId;
    data['course_activity_id'] = this.courseActivityId;
    data['course_med_name'] = this.courseMedName;
    data['course_med_description'] = this.courseMedDescription;
    data['course_cover_image'] = this.courseCoverImage;
    data['course_image'] = this.courseImage;
    data['created_at'] = this.createdAt;
    data['mood_id'] = this.moodId;
    data['mood_title'] = this.moodTitle;
    data['isSleep'] = this.isSleep;
    if (this.medAudio != null) {
      data['med_audio'] = this.medAudio!.toJson();
    }
    return data;
  }
}

class MedAudio {
  String? isDownloaded;
  String? isWatched;
  String? listeningDuration;
  String? minDuration;
  String? maxDuration;
  String? totalDuration;

  MedAudio(
      {this.isDownloaded,
        this.isWatched,
        this.listeningDuration,
        this.minDuration,
        this.maxDuration,
        this.totalDuration});

  MedAudio.fromJson(Map<String, dynamic> json) {
    isDownloaded = json['is_downloaded'];
    isWatched = json['is_watched'];
    listeningDuration = json['listening_duration'];
    minDuration = json['min_duration'];
    maxDuration = json['max_duration'];
    totalDuration = json['total_duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_downloaded'] = this.isDownloaded;
    data['is_watched'] = this.isWatched;
    data['listening_duration'] = this.listeningDuration;
    data['min_duration'] = this.minDuration;
    data['max_duration'] = this.maxDuration;
    data['total_duration'] = this.totalDuration;
    return data;
  }
}

class TotalPages {
  int? totalPages;
  String? currentPage;

  TotalPages({this.totalPages, this.currentPage});

  TotalPages.fromJson(Map<String, dynamic> json) {
    totalPages = json['totalPages'];
    currentPage = json['currentPage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalPages'] = this.totalPages;
    data['currentPage'] = this.currentPage;
    return data;
  }
}