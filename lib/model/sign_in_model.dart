class SignInModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  SignInModel({this.status, this.message, this.data, this.code});
  SignInModel.withError(String err): error = err;

  SignInModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  String? authToken;
  String? name;
  String? emailId;
  String? notificationStatus;
  String? mobileNumber;
  String? countryCode;
  String? isVerify;
  String? profileImage;
  bool? subscription;
  String? buddyLink;

  Data(
      {this.authToken,
        this.name,
        this.emailId,
        this.notificationStatus,
        this.mobileNumber,
        this.countryCode,
        this.isVerify,
        this.profileImage,
        this.subscription,
        this.buddyLink});

  Data.fromJson(Map<String, dynamic> json) {
    authToken = json['auth_token'];
    name = json['name'];
    emailId = json['email_id'];
    notificationStatus = json['notification_status'];
    mobileNumber = json['mobile_number'];
    countryCode = json['country_code'];
    isVerify = json['is_verify'];
    profileImage = json['profile_image'];
    subscription = json['subscription'];
    buddyLink = json['buddy_link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['auth_token'] = this.authToken;
    data['name'] = this.name;
    data['email_id'] = this.emailId;
    data['notification_status'] = this.notificationStatus;
    data['mobile_number'] = this.mobileNumber;
    data['country_code'] = this.countryCode;
    data['is_verify'] = this.isVerify;
    data['profile_image'] = this.profileImage;
    data['subscription'] = this.subscription;
    data['buddy_link'] = this.buddyLink;
    return data;
  }
}