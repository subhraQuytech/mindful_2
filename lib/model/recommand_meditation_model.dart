class RecommandMeditationModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  RecommandMeditationModel({this.status, this.message, this.data, this.code});
  RecommandMeditationModel.withError(String err):error = err;

  RecommandMeditationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  List<CourseMeditation2>? courseMeditation;
  TotalPages? totalPages;

  Data({this.courseMeditation, this.totalPages});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['course_meditation'] != null) {
      courseMeditation = <CourseMeditation2>[];
      json['course_meditation'].forEach((v) {
        courseMeditation!.add(new CourseMeditation2.fromJson(v));
      });
    }
    totalPages = json['total_pages'] != null
        ? new TotalPages.fromJson(json['total_pages'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.courseMeditation != null) {
      data['course_meditation'] =
          this.courseMeditation!.map((v) => v.toJson()).toList();
    }
    if (this.totalPages != null) {
      data['total_pages'] = this.totalPages!.toJson();
    }
    return data;
  }
}

class CourseMeditation2 {
  String? courseMeditationId;
  String? courseTypeId;
  String? courseMedName;
  String? courseMedDescription;
  String? courseImage;
  String? courseCoverImage;
  String? moodId;
  int? isSleep;
  MedAudio? medAudio;

  CourseMeditation2(
      {this.courseMeditationId,
        this.courseTypeId,
        this.courseMedName,
        this.courseMedDescription,
        this.courseImage,
        this.courseCoverImage,
        this.moodId,
        this.isSleep,
        this.medAudio});

  CourseMeditation2.fromJson(Map<String, dynamic> json) {
    courseMeditationId = json['course_meditation_id'];
    courseTypeId = json['course_type_id'];
    courseMedName = json['course_med_name'];
    courseMedDescription = json['course_med_description'];
    courseImage = json['course_image'];
    courseCoverImage = json['course_cover_image'];
    moodId = json['mood_id'];
    isSleep = json['isSleep'];
    medAudio = json['med_audio'] != null
        ? new MedAudio.fromJson(json['med_audio'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['course_meditation_id'] = this.courseMeditationId;
    data['course_type_id'] = this.courseTypeId;
    data['course_med_name'] = this.courseMedName;
    data['course_med_description'] = this.courseMedDescription;
    data['course_image'] = this.courseImage;
    data['course_cover_image'] = this.courseCoverImage;
    data['mood_id'] = this.moodId;
    data['isSleep'] = this.isSleep;
    if (this.medAudio != null) {
      data['med_audio'] = this.medAudio!.toJson();
    }
    return data;
  }
}

class MedAudio {
  String? totalDuration;
  String? minDuration;
  String? maxDuration;
  String? listeningDuration;

  MedAudio(
      {this.totalDuration,
        this.minDuration,
        this.maxDuration,
        this.listeningDuration});

  MedAudio.fromJson(Map<String, dynamic> json) {
    totalDuration = json['total_duration'];
    minDuration = json['min_duration'];
    maxDuration = json['max_duration'];
    listeningDuration = json['listening_duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_duration'] = this.totalDuration;
    data['min_duration'] = this.minDuration;
    data['max_duration'] = this.maxDuration;
    data['listening_duration'] = this.listeningDuration;
    return data;
  }
}

class TotalPages {
  int? totalPages;
  String? currentPage;

  TotalPages({this.totalPages, this.currentPage});

  TotalPages.fromJson(Map<String, dynamic> json) {
    totalPages = json['totalPages'];
    currentPage = json['currentPage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalPages'] = this.totalPages;
    data['currentPage'] = this.currentPage;
    return data;
  }
}