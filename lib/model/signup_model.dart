class SignUpModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  SignUpModel({this.status, this.message, this.data, this.code});
  SignUpModel.withError(String err): error = err;

  SignUpModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  String? authToken;
  String? name;
  String? profileImage;
  String? otpCode;
  String? buddyLink;

  Data(
      {this.authToken,
        this.name,
        this.profileImage,
        this.otpCode,
        this.buddyLink});

  Data.fromJson(Map<String, dynamic> json) {
    authToken = json['auth_token'];
    name = json['name'];
    profileImage = json['profile_image'];
    otpCode = json['otp_code'];
    buddyLink = json['buddy_link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['auth_token'] = this.authToken;
    data['name'] = this.name;
    data['profile_image'] = this.profileImage;
    data['otp_code'] = this.otpCode;
    data['buddy_link'] = this.buddyLink;
    return data;
  }
}