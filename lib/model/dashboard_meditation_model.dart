class DashBoardMeditationModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  DashBoardMeditationModel({this.status, this.message, this.data, this.code});
  DashBoardMeditationModel.withError(String err):error = err;
  DashBoardMeditationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  CourseMeditation? courseMeditation;
  TotalPages? totalPages;
  String? isUserValidSubscription;

  Data({this.courseMeditation, this.totalPages, this.isUserValidSubscription});

  Data.fromJson(Map<String, dynamic> json) {
    courseMeditation = json['course_meditation'] != null
        ? new CourseMeditation.fromJson(json['course_meditation'])
        : null;
    totalPages = json['total_pages'] != null
        ? new TotalPages.fromJson(json['total_pages'])
        : null;
    isUserValidSubscription = json['isUserValidSubscription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.courseMeditation != null) {
      data['course_meditation'] = this.courseMeditation!.toJson();
    }
    if (this.totalPages != null) {
      data['total_pages'] = this.totalPages!.toJson();
    }
    data['isUserValidSubscription'] = this.isUserValidSubscription;
    return data;
  }
}

class CourseMeditation {
  String? courseMeditationId;
  String? courseTypeId;
  String? courseMedName;
  String? courseMedDescription;
  String? moodId;
  String? courseImage;
  String? courseCoverImage;
  int? isSleep;
  MedAudio? medAudio;

  CourseMeditation(
      {this.courseMeditationId,
        this.courseTypeId,
        this.courseMedName,
        this.courseMedDescription,
        this.moodId,
        this.courseImage,
        this.courseCoverImage,
        this.isSleep,
        this.medAudio});

  CourseMeditation.fromJson(Map<String, dynamic> json) {
    courseMeditationId = json['course_meditation_id'];
    courseTypeId = json['course_type_id'];
    courseMedName = json['course_med_name'];
    courseMedDescription = json['course_med_description'];
    moodId = json['mood_id'];
    courseImage = json['course_image'];
    courseCoverImage = json['course_cover_image'];
    isSleep = json['isSleep'];
    medAudio = json['med_audio'] != null
        ? new MedAudio.fromJson(json['med_audio'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['course_meditation_id'] = this.courseMeditationId;
    data['course_type_id'] = this.courseTypeId;
    data['course_med_name'] = this.courseMedName;
    data['course_med_description'] = this.courseMedDescription;
    data['mood_id'] = this.moodId;
    data['course_image'] = this.courseImage;
    data['course_cover_image'] = this.courseCoverImage;
    data['isSleep'] = this.isSleep;
    if (this.medAudio != null) {
      data['med_audio'] = this.medAudio!.toJson();
    }
    return data;
  }
}

class MedAudio {
  String? maxDuration;
  String? minDuration;
  String? totalDuration;
  String? listeningDuration;

  MedAudio(
      {this.maxDuration,
        this.minDuration,
        this.totalDuration,
        this.listeningDuration});

  MedAudio.fromJson(Map<String, dynamic> json) {
    maxDuration = json['max_duration'];
    minDuration = json['min_duration'];
    totalDuration = json['total_duration'];
    listeningDuration = json['listening_duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['max_duration'] = this.maxDuration;
    data['min_duration'] = this.minDuration;
    data['total_duration'] = this.totalDuration;
    data['listening_duration'] = this.listeningDuration;
    return data;
  }
}

class TotalPages {
  String? totalPages;
  String? currentPage;

  TotalPages({this.totalPages, this.currentPage});

  TotalPages.fromJson(Map<String, dynamic> json) {
    totalPages = json['totalPages'];
    currentPage = json['currentPage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalPages'] = this.totalPages;
    data['currentPage'] = this.currentPage;
    return data;
  }
}