class ChangeBuddyLinkModel {
  bool? status;
  String? message;
  Data? data;
  int? code;
  String? error;

  ChangeBuddyLinkModel({this.status, this.message, this.data, this.code});
  ChangeBuddyLinkModel.withError(String err): error = err;

  ChangeBuddyLinkModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  String? buddyLink;

  Data({this.buddyLink});

  Data.fromJson(Map<String, dynamic> json) {
    buddyLink = json['buddy_link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['buddy_link'] = this.buddyLink;
    return data;
  }
}